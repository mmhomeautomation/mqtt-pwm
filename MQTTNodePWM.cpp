/*
 * MQTTNodePWM.cpp
 *
 *  Created on: 05.01.2017
 *      Author: maddin
 */

#include <Arduino.h>

#include <modules/debug-utils/debug-utils.hpp>

#include "MQTTNodePWM.hpp"
#include "../mqtt-base/MQTTNodeObservable.hpp"
#include "../mqtt-base/MQTTBase.hpp"

MQTTNodePWM::MQTTNodePWM() : MQTTNodeSimple() {
}

MQTTNodePWM::MQTTNodePWM(String name, uint8_t pin) : MQTTNodeSimple(name) {
	this->pin = pin;
	current = 0;
	target = 0;
}

void MQTTNodePWM::timer10ms() {
	if(pin != UINT8_MAX) {
		if(current < target) {
			current += stepsizePer10ms;
			if(current > target) current = target;
		} else {
			if(current > target) {
				current -= stepsizePer10ms;
				if(current < target) current = target;
			}
		}

		analogWrite(pin, (uint16_t)current);
	}
}

MQTTNodeObservable * MQTTNodePWM::addConfigNode(String name, bool attach) {
	String configNodeName = this->channelName + "_" + name;
	MQTTNodeObservable * obs = new MQTTNodeObservable(configNodeName);
	obs->setID(name);
	if(attach) obs->attachObserver( (MQTTObserver*)this );
	( (MQTTBase *)base )->addNode(obs);
	return obs;
}

MQTTNodeObservable * MQTTNodePWM::addConfigNode(String name, String initValue, bool attach) {
	MQTTNodeObservable * obs = addConfigNode(name, attach);
	obs->setValue(initValue, true);
	return obs;
}

void MQTTNodePWM::updateTarget(uint16_t newTarget) {
	enabledTarget = newTarget;

	if(enabled) {
		target = enabledTarget;
	} else target = 0;

	if(config_fademode == MQTTNODEPWM_FADEMODE_CONSTANT_TIME) {
		uint16_t diff = (current > target) ? (current - target) : (target - current);
		stepsizePer10ms = ( ( (float)diff * 10.0 ) / (float)config_constanttime_ms );
	} else stepsizePer10ms = config_stepsize_ms * ( (float)PWMRANGE / 100.0 );

	_printf("[PWMNode '%s'] newTarget = '%u', target = '%u', sSize = '%u' ('%u', '%u')\n", getName().c_str(), newTarget, (uint16_t)target, (uint16_t)(stepsizePer10ms * 10), config_fademode, config_constanttime_ms);
}

void MQTTNodePWM::updateTarget(String newTarget) {
	uint16_t value = (uint16_t)newTarget.toInt();
	updateTarget(value);
}

void MQTTNodePWM::update(String id, String newValue) {
	_printf( "[PWMNode '%s'] '%s' -> '%s'\n", getName().c_str(), id.c_str(), newValue.c_str() );
	if(id == "enabled") {
		if(newValue == "off") {
			enabled = false;
			//target = 0;
		} else if(newValue == "on") {
			enabled = true;
			//target = enabledTarget;
		}else if(newValue == "toggle"){
			this->enabled = !this->enabled;
		}
		updateTarget(enabledTarget);
	} else if(id == "level") {
		uint16_t value = (uint16_t)newValue.toInt();
		updateTarget( map(value, 0, 100, 0, PWMRANGE) );
	} else if(id == "fadeTime") {
		config_constanttime_ms = (uint16_t)newValue.toInt();
	} else if(id == "fadeSteps") {
		config_stepsize_ms = newValue.toFloat();
	} else if(id == "fadeMode") {
		config_fademode = (newValue == "constantTime" ? MQTTNODEPWM_FADEMODE_CONSTANT_TIME : MQTTNODEPWM_FADEMODE_CONSTANT_STEPSIZE);
		updateTarget(enabledTarget);
	} else {
		_printf("UNKNOWN!\n");
		return;
	}

	if(stateNode) {
		if(enabled && target) {
			stateNode->setValue("on", true);
		} else stateNode->setValue("off", true);
	}

}

void MQTTNodePWM::setupNode() {
	enabledNode = addConfigNode("enabled", enabled ? "on" : "off");
	levelNode = addConfigNode( "level", String( map(target, 0, PWMRANGE, 0, 100) ) );
	stateNode = addConfigNode("state", (enabled && target) ? "on" : "off", false);
	addConfigNode( "fadeTime", String(config_constanttime_ms) );
	addConfigNode( "fadeSteps", String(config_stepsize_ms, 1) );
	addConfigNode( "fadeMode", String(config_fademode == MQTTNODEPWM_FADEMODE_CONSTANT_TIME ? "constantTime" : "constantSteps") );
	updateTarget(target);
}
