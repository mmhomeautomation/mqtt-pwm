/*
 * MQTTNodePWM.h
 *
 *  Created on: 05.01.2017
 *      Author: maddin
 */

#ifndef SRC_MODULES_MQTT_PWM_MQTTNODEPWM_H_
#define SRC_MODULES_MQTT_PWM_MQTTNODEPWM_H_

#include <modules/mqtt-base/MQTTNodeObservable.hpp>

enum mqttNodePWM_fademode_e {
	MQTTNODEPWM_FADEMODE_CONSTANT_TIME,
	MQTTNODEPWM_FADEMODE_CONSTANT_STEPSIZE
};

class MQTTNodePWM : public MQTTNodeSimple, MQTTObserver {
	uint16_t enabledTarget = 0;
	float current = 0.0, target = 0.0, stepsizePer10ms = 1.0;
	uint8_t pin = UINT8_MAX;

	bool enabled = true;

	MQTTNodeObservable * levelNode = NULL;
	MQTTNodeObservable * enabledNode = NULL;
	MQTTNodeObservable * stateNode = NULL;

	enum mqttNodePWM_fademode_e config_fademode = MQTTNODEPWM_FADEMODE_CONSTANT_TIME;
	float config_stepsize_ms = 0.1;
	uint16_t config_constanttime_ms = 250;

	void updateTarget(uint16_t newTarget);
	void updateTarget(String newTarget);

	public:
		MQTTNodePWM();
		MQTTNodePWM(String name, uint8_t pin);

		void timer10ms() override;
		MQTTNodeObservable * addConfigNode(String name, bool attach = true);
		MQTTNodeObservable * addConfigNode(String name, String initValue, bool attach = true);
		void update(String id, String newValue) override;
		void setupNode() override;
};

#endif	/* SRC_MODULES_MQTT_PWM_MQTTNODEPWM_H_ */
